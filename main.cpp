#include <iostream>
#include "opencv2/opencv.hpp"
#include <dirent.h>
#include <queue>
#include <limits>

using namespace std;
using namespace cv;

// scaling coefficient for image preprocessing
const double scale = 0.1;


// images of objects to search
vector<Mat> objects;

// masks of objects to search
vector<Mat> objectMasks;

// object filenames
vector<string> objectNames;

// positions of objects found in the previous frame
vector<Point> foundObjectsPositions;

// objects approximate contour found in the previous frame
vector<Mat> foundObjectsContour;


// stores the position in image and template match result
struct pos_value {
  double val;
  Point pos;
};

// reads object images from the selected directory
void readObjects(string path);

// gets the minimal region in image which contains all nonzero values
Rect getNonzeroRoi(Mat &mat);

// scans the frame and executes template matching
void scanFrame(Mat &frame, Mat &templ, Mat &mask, Mat &searchArea, Mat &result);

// executes template matching
void templateMatch(Mat &m, Mat &templ, Mat &mask, pos_value &pv, Mat &searchArea);

// removes objects from the search area which were already classified
void removeFoundObjects(Mat &searchArea);

// gets the accurate contour of the object
Mat getAccurateContour(const Mat &frame, const Mat &searchArea, const Rect &r);

// draws information and contour of the found object
void drawObjectInfo(const Mat &resultFrame, const Mat &searchArea, int i, const Rect &r, const Mat &mt);

// main function
// arguments:
// path to the folder with images
// filename of the video
int main(int argc, char *argv[]) {
  if (argc != 3) {
    cout << "Command line arguments:" << endl;
    cout << "<path to the folder with images>" << endl;
    cout << "<filename of the video>" << endl;
    return 0;
  }
  Mat frame, resultFrame, prevFrame, prevFrameScaled, searchArea, searchAreaScaled;
  string objectsPath = argv[1];
  string videoPath = argv[2];

  // read object images
  readObjects(objectsPath);

  // open video
  VideoCapture vid(videoPath);
  if (!vid.isOpened()) {
    return 0;
  }

  while (1) {
    vid >> frame;
    if (frame.empty())
      break;

    Mat frameScaled;

    // get scaled frame
    resize(frame, frameScaled, Size(0, 0), scale, scale);

    // clone frame for results displaying
    resultFrame = frame.clone();

    // convert frame to grayscale
    cvtColor(frame, frame, COLOR_BGR2GRAY);
    Mat grayscaleFrame;
    cvtColor(frameScaled, grayscaleFrame, COLOR_BGR2GRAY);

    // if previous frame is empty
    if (prevFrame.empty()) {
      // save previous frame
      prevFrameScaled = grayscaleFrame;
      prevFrame = frame;
      continue;
    }

    // get changes between frames
    searchAreaScaled = abs(grayscaleFrame - prevFrameScaled);
    searchArea = abs(frame - prevFrame);

    // save previous frame
    prevFrameScaled = grayscaleFrame;
    prevFrame = frame;

    // threshold changes
    threshold(searchAreaScaled, searchAreaScaled, 70, 255, THRESH_BINARY);
    threshold(searchArea, searchArea, 70, 255, THRESH_BINARY);

    // removes objects from the search area which were already classified
    removeFoundObjects(searchAreaScaled);

    for (int i = 0; i < objects.size(); i++) {
      // if object was not found in the previous frame
      if (foundObjectsPositions[i].x == -1 && foundObjectsPositions[i].y == -1) {
        pos_value pv;

        // execute template matching
        templateMatch(grayscaleFrame, objects[i], objectMasks[i], pv, searchAreaScaled);

        int h = objects[i].rows;
        int w = objects[i].cols;

        // if template match result is good enough
        if (pv.val > 0.82) {

          // get accurate contour
          int k = (int) (1 / scale);
          Point pt = Point(pv.pos.x * k, pv.pos.y * k);
          Rect r = Rect(pt.x, pt.y, w * k, h * k);
          Mat mt = getAccurateContour(frame, searchArea, r);

          // draw object information and contour
          drawObjectInfo(resultFrame, searchArea, i, r, mt);

          // save found object position and approximate contour
          foundObjectsPositions[i] = pt;
          Mat contour = searchArea(r).clone();
          foundObjectsContour[i] = contour;
        }
      }
      else {
        // create empty frame
        Mat emptyFrame = Mat(frame.rows, frame.cols, CV_8UC1, Scalar(0));

        // rectangle of the object found in the previous frame
        int w = foundObjectsContour[i].cols;
        int h = foundObjectsContour[i].rows;
        int x = foundObjectsPositions[i].x;
        int y = foundObjectsPositions[i].y;
        Rect rect = Rect(x, y, w, h);

        // rectangle which might contain new location of the object
        int dx = (int) (w * 0.05);
        int dy = (int) (h * 0.05);
        Rect increasedRect = Rect(max(x - dx, 0), max(y - dy, 0), w + dx * 2, h + dy * 2);
        if (increasedRect.x + increasedRect.width > searchArea.cols)
          increasedRect.width = searchArea.cols - increasedRect.x;
        if (increasedRect.y + increasedRect.height > searchArea.rows)
          increasedRect.height = searchArea.rows - increasedRect.y;

        // copy approximate contour of the found object from the previous frame
        Mat frameROI = emptyFrame(rect);
        foundObjectsContour[i].copyTo(frameROI);

        // get intersection with the search area
        emptyFrame = emptyFrame & searchArea;

        // convert part of the search area to BGR
        Mat searchAreaCopy;
        cvtColor(searchArea, searchAreaCopy, COLOR_GRAY2BGR);
        Mat searchAreaDrawRect = searchAreaCopy(increasedRect);

        dx = rect.x - increasedRect.x;
        dy = rect.y - increasedRect.y;
        bool hasObject = false;
        for (int i = 0; i < h; i++)
          for (int j = 0; j < w; j++) {
            // if the point is in intersection of the previous contour and the search area
            uchar val = frameROI.at<uchar>(i, j);
            if (val > 0) {
              hasObject = true;
              Vec3b saVal = searchAreaDrawRect.at<Vec3b>(i + dy, j + dx);
              if (saVal != Vec3b(0, 0, 255))
                // set the connected component as the part of the new position of the object
                floodFill(searchAreaDrawRect, Point(j + dx, i + dy), Vec3b(0, 0, 255));
            }
          }

        // if intersection is not empty
        if (hasObject) {

          // get the connected components with the new position of the objet
          Mat channels[3];
          split(searchAreaCopy, channels);
          channels[2] -= channels[1];

          // get bounding box of the object
          rect = getNonzeroRoi(channels[2]);

          // check that the object is not too large
          int objectHeight = (int) (objects[i].rows / scale);
          int objectWidth = (int) (objects[i].cols / scale);
          if (abs(rect.height - objectHeight) < 0.2 * objectHeight &&
              abs(rect.width - objectWidth) < 0.2 * objectWidth) {

            // get accurate contour
            Mat mt = getAccurateContour(frame, channels[2], rect);

            // draw object information and contour
            drawObjectInfo(resultFrame, channels[2], i, rect, mt);

            // save found object position and approximate contour
            foundObjectsPositions[i] = Point(rect.x, rect.y);
            Mat contour = searchArea(rect).clone();
            foundObjectsContour[i] = contour;
          }
          else {
            foundObjectsPositions[i] = Point(-1, -1);
          }

        }
        else {
          foundObjectsPositions[i] = Point(-1, -1);
        }
      }
    }

    // display result
    resize(resultFrame, resultFrame, Size(0, 0), 0.5, 0.5);
    imshow("Object detect", resultFrame);
    waitKey(3);
  }
  return 0;
}

// draws information and contour of the found object
void drawObjectInfo(const Mat &resultFrame, const Mat &searchArea, int i, const Rect &r, const Mat &mt) {
  Vec3b color;
  if (i == 0)
    color = Vec3b(0, 0, 255);
  else if (i == 1)
    color = Vec3b(0, 255, 0);
  else if (i == 2)
    color = Vec3b(255, 0, 0);
  else
    color = Vec3b(255, 255, 0);

  // draw red contour
  Mat chan1 = Mat(mt.rows, mt.cols, CV_8UC1, Scalar(0));
  Mat channels[3];
  channels[0] = chan1;
  channels[1] = chan1;
  channels[2] = mt;
  Mat redContour;
  merge(channels, 3, redContour);
  Mat m = resultFrame(r);
  m = m + redContour;

  // draw filename
  Point pt = Point(r.x, r.y);
  putText(resultFrame, objectNames[i], pt, FONT_HERSHEY_COMPLEX, 1, color);
}

// removes objects from the search area which were already classified
void removeFoundObjects(Mat &searchArea) {
  for (int i = 0; i < foundObjectsPositions.size(); i++)
    if (!(foundObjectsPositions[i].x == -1 && foundObjectsPositions[i].y == -1)) {
      Rect r = Rect((int) (foundObjectsPositions[i].x * scale),
                    (int) (foundObjectsPositions[i].y * scale),
                    (int) (foundObjectsContour[i].cols * scale),
                    (int) (foundObjectsContour[i].rows * scale));
      Mat searchAreaRect = searchArea(r);
      searchAreaRect.setTo(Scalar(0));
    }
}

// gets the minimal region in image which contains all nonzero values
Rect getNonzeroRoi(Mat &mat) {
  int minX = mat.cols;
  int minY = mat.rows;
  int maxX = 0;
  int maxY = 0;

  for (int i = 0; i < mat.rows; i++)
    for (int j = 0; j < mat.cols; j++) {
      uchar val = mat.at<uchar>(i, j);
      if (val > 0) {
        minX = min(minX, j);
        maxX = max(maxX, j);
        minY = min(minY, i);
        maxY = max(maxY, i);
      }
    }
  return Rect(minX, minY, maxX - minX + 1, maxY - minY + 1);
}

// gets the accurate contour of the object
Mat getAccurateContour(const Mat &frame, const Mat &searchArea, const Rect &r) {
  Mat mt = frame(r).clone();
  Canny(mt, mt, 100, 300);

  Mat searchAreaROI = searchArea(r);
  dilate(searchAreaROI, searchAreaROI, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

  mt = mt & searchAreaROI;
  return mt;
}

// scans the frame and executes template matching
void scanFrame(Mat &frame, Mat &templ, Mat &mask, Mat &searchArea, Mat &result) {
  int i = 0;
  int j = 0;
  // position for the next scan step, determined by the position of
  // non zero points of the search area
  int mxNonZeroI = 0;
  while (i < frame.rows - templ.rows) {
    bool scanRowContainsObjects = false;

    while (j < frame.cols - templ.cols) {

      // the amount of points which are differ from the previous frame,
      // determined by non zero points in the search area
      double nonZeroAmount = 0.0;
      for (int k = 0; k < templ.rows; k++)
        for (int l = 0; l < templ.cols; l++) {
          uchar val = searchArea.at<uchar>(i + k, j + l);
          if (val > 0) {
            nonZeroAmount += 1;
            mxNonZeroI = max(mxNonZeroI, i);
          }
        }

      // of the amount of points is large enough
      nonZeroAmount /= templ.cols * templ.rows;
      if (nonZeroAmount > 0.07) {
        scanRowContainsObjects = true;

        // execute template matching
        Mat res(1, 1, CV_32FC1);
        Rect r = Rect(j, i, templ.cols, templ.rows);
        matchTemplate(frame(r), templ, res, TM_CCORR_NORMED, mask);
        result.at<float>(i, j) = res.at<float>(0, 0);
        j++;
      }
      else
        j += templ.cols;
    }
    if (scanRowContainsObjects)
      i++;
    else {
      if (mxNonZeroI > 0)
        i += mxNonZeroI;
      else
        i += templ.rows;
    }
    j = 0;
  }
}

// executes template matching
void templateMatch(Mat &m, Mat &templ, Mat &mask, pos_value &pv, Mat &searchArea) {
  Mat result(m.rows - templ.rows + 1, m.cols - templ.cols + 1, CV_32FC1, Scalar(0));
  scanFrame(m, templ, mask, searchArea, result);
  double mn, mx;
  Point min_loc, max_loc;
  minMaxLoc(result, &mn, &mx, &min_loc, &max_loc);
  pv.pos = max_loc;
  pv.val = mx;
}

// reads object images from the selected directory
void readObjects(string path) {
  DIR *dir;
  dirent *ent;
  dir = opendir(path.c_str());
  string currentFile;
  int k = 0;
  while ((ent = readdir(dir)) != NULL) {
    currentFile = ent->d_name;
    if (currentFile == "." || currentFile == "..")
      continue;

    // read object image
    objects.push_back(imread(path + currentFile, IMREAD_UNCHANGED));

    // get object mask
    Mat mask;
    threshold(objects[k], mask, 0, 255, THRESH_BINARY);
    objectMasks.push_back(mask.clone());

    // resize object and mask
    resize(objects[k], objects[k], Size(0, 0), scale, scale);
    resize(objectMasks[k], objectMasks[k], Size(0, 0), scale, scale);

    // convert to grayscale object and mask
    cvtColor(objectMasks[k], objectMasks[k], COLOR_BGR2GRAY);
    cvtColor(objects[k], objects[k], COLOR_BGR2GRAY);

    // save object name
    objectNames.push_back(currentFile);

    // initialize found object positions and approximate contours
    foundObjectsPositions.push_back(Point(-1, -1));
    Mat empty;
    foundObjectsContour.push_back(empty);
    k++;
  }
  closedir(dir);
}